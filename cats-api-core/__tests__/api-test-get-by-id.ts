import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Тестовыйкотданисимо', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('API core (котики)', () => {
	beforeAll(async () => {
		try {
			const add_cat_response = await HttpClient.post('core/cats/add', {
				responseType: 'json',
				json: { cats },
			});
			if ((add_cat_response.body as CatsList).cats[0].id) {
				catId = (add_cat_response.body as CatsList).cats[0].id;
			} else throw new Error('Не получилось получить id тестового котика!');
		} catch (error) {
			throw new Error('Не удалось создать котика для автотестов!');
		}
	});

	afterAll(async () => {
		await HttpClient.delete(`core/cats/${catId}/remove`, {
			responseType: 'json',
		});
	});

	it('Поиск кота по существующему id', async () => {
		const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
			responseType: 'json',
		});
		expect(response.statusCode).toEqual(200);

		expect(response.body).toEqual({
			cat: expect.objectContaining({
				id: catId,
				name: cats[0].name,
				description: cats[0].description,
				gender: cats[0].gender,
			}),
		});
	});

	it('Поиск кота по некорректному id вызовет ошибку', async () => {
		await expect(
			HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
				responseType: 'json',
			})
		).rejects.toThrowError('Response code 400 (Bad Request)');
	});
});

