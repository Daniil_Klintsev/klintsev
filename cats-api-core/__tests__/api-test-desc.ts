import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Тестовыйкотданисимо', description: '', gender: 'male' }];

let catId;

const desc = "Описание моего милейшего котика";

const HttpClient = Client.getInstance();

describe('API core (котики)', () => {
	beforeEach(async () => {
		try {
			const add_cat_response = await HttpClient.post('core/cats/add', {
				responseType: 'json',
				json: { cats },
			});
			if ((add_cat_response.body as CatsList).cats[0].id) {
				catId = (add_cat_response.body as CatsList).cats[0].id;
			} else throw new Error('Не получилось получить id тестового котика!');
		} catch (error) {
			throw new Error('Не удалось создать котика для автотестов!');
		}
	});

	afterEach(async () => {
		await HttpClient.delete(`core/cats/${catId}/remove`, {
			responseType: 'json',
		});
	});

	it('Проверка добавления описания котику', async () => {
		const response = await HttpClient.post(`core/cats/save-description`, {
			responseType: 'json',
			json: {
				"catId": catId,
				"catDescription": desc,
			},
		});
		expect(response.statusCode).toEqual(200);

		expect(response.body).toMatchObject({
			id: catId,
			description: desc,
		});
	});
});